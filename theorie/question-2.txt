Question 2 (10 pts)
-------------------

a. (2.5 pts) Faites la liste de ce qui ne va pas dans le code ci-dessous.

b. (7.5 pts) Proposez une réécriture.

- cut here ----------------------------------------------------------------------
import java.io.*;

class main
{
   public static void main(String[] args)
   {
      int[][] matrix = new int[3][3];
      //initialize a matrix
      for (int i=1; i < 3; i++)
      {
         for (int j=0; j < 3; j++)
         {
            matrix[i][j] = i + (j++);
         }
      }
      //find if a number is contained in the matrix and output
      boolean found = false;
      for (int i=0; i < 3; i++)
      {
         for (int j=0; j <= 2; j++)
         {
            if (matrix[i][j] ==  2)
            {
               System.out.println("2 can be found at position " + (i+1) + '-' + (j+1));
               found = true;
               break;
            }
         }
         if (found)
         {
            break;
         }
      }
      //output the matrix
      System.out.println("The matrix is:");
      for (int i=0; i <= 2; i++)
      {
         for (int j=0; j <=2; j++)
         {
            System.out.print(matrix[i][j] + " ");
         }
         System.out.println();
      }
      System.out.println();
   }
}
- cut here ----------------------------------------------------------------------
A)Le code devrais être séparé en fonctions. 
Le premier i est mal initialisé.
j <=2 devrait être j < 3 pour être constant avec le reste du code.
La partie du code qui cherche 2 devrais avoir une variable constante facilement changeable ou devrait être un paramètre.
L'indentation devrais suivre une norme.
L'affichage de l'endroit où est trouver 2 peut être mélangeant.


B)
- cut here ----------------------------------------------------------------------
import java.io.*;

class main{
	public static void main(String[] args){
		int[][] matrix = initializeMatrix(3,3);
		findValue(2, matrix);
		printMatrix(matrix);
	}
	//function that creates a matrix based on height and width
	public static int[][] initializeMatrix(height, width){
		int[][] matrix = new int[height][width];
		//initialize a matrix
		for (int i=0; i < height; i++){
			for (int j=0; j < width; j++){
				matrix[i][j] = i + (j++);
			}
		}
		return matrix;
	}
	//function that find an int into a matrix
	public static boolean findValue(int value,int[][] matrix){
		boolean found = false;
		for (int i=0; i < 3; i++){
			for (int j=0; j < 3; j++){
				if (matrix[i][j] ==  value){
					System.out.println(value+" can be found at position " + (i) + '-' + (j));
					found = true;
					return found;
				}
			}
		}
		return found;
	}
	//function that prints a matrix
	public static void printMatrix(int[][] matrix){
		System.out.println("The matrix is:");
		for (int i=0; i < 3; i++){
			for (int j=0; j < 3; j++){
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
}
- cut here ----------------------------------------------------------------------

