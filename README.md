
420-504 -- EXAMEN INTRA -- Automne 2016
---------------------------------------

PROCÉDURE

- Faites un FORK de ce dépot en le renommant intra-504-<votre prénom>.
- Donnez accès à l'usager ngorse.
- Faites un git clone de ce dépot sur votre ordinateur.
- Faites votre examen.
- Une fois votre examen terminé, assurez-vous que vous avez bien fait un commit de tous les fichiers nécessaires.


NOTES
    - Si vous avez une incertitude, faites une supposition et documentez-la.
    - Toute documentation permise.
    - Accès à internet autorisé.
    - Communication interdite.
    - Toutes les réponses doivent être de votre composition. Tout copier-coller venant du web sera considéré comme du plagiat.
    

PARTIE THÉORIQUE (50 pts)
-------------------------

Répondre aux questions 1 à 5 dans les fichiers prévus à cet effet dans le répertoire theorie/. 


PARTIE PRATIQUE (50 pts)
------------------------

Suivez les directives de l'énoncé se trouvant dans pratique/enonce.txt.
