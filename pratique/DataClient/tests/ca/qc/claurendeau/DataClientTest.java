package ca.qc.claurendeau;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import ca.qc.claurendeau.exceptions.DataException;
import ca.qc.claurendeau.exceptions.DatabaseException;
import ca.qc.claurendeau.exceptions.NetworkException;

import static org.junit.Assert.*;

import org.junit.Before;

public class DataClientTest {

	@Mock
	DataServer dataServer;
	DataClient dataClient;
	
	
	@Before
	public void before(){
		dataServer = new DataServer();
		dataClient = new DataClient(dataServer);
	}
	
	@Test
	public void testProcessData() throws DatabaseException, DataException, NetworkException{
		assertEquals(2, dataClient.processData(10));
	}
	@Test
	public void testProcessDataFail() throws DatabaseException, DataException, NetworkException{
		assertEquals(-1, dataClient.processData(1000000000));
	}
	
	@Test
	public void testCalculateOuput(){
		assertEquals(20, dataClient.calculateOutput(10, 10));
	}
	
	@Test
	public void testCalculatVoltage(){
		assertEquals(10*10, dataClient.calculateVoltage(10, 10));
	}
	
	@Test
	public void testExtractCSV() throws DataException{
		//Mockito.when(dataServer.getCSVData()).thenReturn("10,20,30,40");
		assertTrue(dataClient.extractCSV());
	}
	
	@Test
	public void testFinalizeTransaction() throws DatabaseException{
		dataClient.initiateNewTransaction(10);
		assertTrue(dataClient.finalizeTransaction());
	}
	
	@Test
	public void testIniTializeTransaction() throws DatabaseException{
		assertTrue(dataClient.initiateNewTransaction(10));
	}
}
