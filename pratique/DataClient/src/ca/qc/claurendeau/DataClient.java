package ca.qc.claurendeau;

import java.util.Arrays;
import java.util.List;

import ca.qc.claurendeau.exceptions.DataException;
import ca.qc.claurendeau.exceptions.DatabaseException;
import ca.qc.claurendeau.exceptions.NetworkException;

public class DataClient
{
	DataServer dataServer;
	int output = -1;
	int coefficient = -1;
	int IDCSV = -1;
	int inputLeft = -1;
	int inputRight = -1;
	
	public DataClient(DataServer dataServer)
	{
		this.dataServer = dataServer;
	}

	public int processData(int entryID) throws DatabaseException, DataException, NetworkException
	{	
		int voltage = -1;
		boolean transactionInitiated = false;
		while(transactionInitiated){
			transactionInitiated = initiateNewTransaction(entryID);
		}
		
		boolean csvExtracted = false;
		while(csvExtracted){
			csvExtracted = extractCSV();
		}
		
		output = calculateOutput(inputLeft, inputRight);
		
		if(dataServer.okToSendVoltage(output, coefficient)){
			dataServer.sendVoltage(output, coefficient);
			voltage = calculateVoltage(output, coefficient);
		}
		
		boolean transactionFinalized = false;
		while(transactionFinalized){
			transactionFinalized = finalizeTransaction();
		}
		
		return voltage;	
	}
	public boolean finalizeTransaction() throws DatabaseException{
		boolean flag = false;
		
		dataServer.finalizeTransaction();
		flag = true;
		return flag;
	}
	public boolean initiateNewTransaction(int entryID) throws DatabaseException{
		boolean flag = false;
		
		dataServer.initiateNewTransaction(entryID);
		flag = true;
		return flag;
	}
	public int calculateVoltage(int output, int coefficient){
		return output * coefficient;
	}
	public int calculateOutput(int inputLeft, int inputRight){
		return inputLeft+inputRight;
	}
	public boolean extractCSV() throws DataException{
		boolean flag = false;
		
		String csv = dataServer.getCSVData();
		
		List<String> csvList = Arrays.asList(csv.split(","));
		IDCSV = Integer.parseInt(csvList.get(0));
		inputLeft = Integer.parseInt(csvList.get(1));
		inputRight = Integer.parseInt(csvList.get(2));
		coefficient = Integer.parseInt(csvList.get(3));
		
		if (IDCSV!=-1 && inputLeft != -1 && inputRight!= -1 && coefficient!= -1) {
			flag = true;
		}
		return flag;
	}
}
